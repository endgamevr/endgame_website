function makeNameRow(name, icon) {
	return `<div class="twitterWidgetRow"><img src="${icon}" class="twitterIcon" /><strong>${name}</strong></div>`;
}

function makeUsernameRow(username) {
	return `<div class="twitterWidgetRow secondary"><a href="https://www.twitter.com/endgamevr">@${username}</a></div>`;
}

function makeDateRow(dateString) {
	return `<div class="twitterWidgetRow secondary">${dateString}</div>`;
}

function makeTweetContent(text) {
	return `<div class="twitterWidgetRow">${text}</div>`;
}

function makeTweet({name, username, dateString, tweetUrl, text, id, icon}) {
	return `<div class="twitterWidgetTweet" key="${id}">
${makeNameRow(name, icon)}
${makeUsernameRow(username)}
${makeDateRow(dateString)}
${makeTweetContent(text)}
</div>`;
}

function makeTwitterWidget(tweets) {
	const tweetDivs = tweets.map((tweet) => {
		return makeTweet(tweet);
	});
	return `<div class="twitterWidgetBox">
${tweetDivs.join('')}
</div>`;
}

function parseTweets(allTweetsString) {
	const tweets = JSON.parse(allTweetsString);
	if (tweets.error) {
		console.warn('Error when trying to retrieve tweets:', tweets.error);
		return;
	}
	return tweets.map((tweet) => {
		const link = parseForUrl(tweet.text);
		return {
			id: tweet.id,
			text: addLinkToText(tweet.text, link),
			isReply: tweet.in_reply_to_status_id !== null,
			name: tweet.user.name,
			username: tweet.user.screen_name,
			dateString: tweet.created_at.split(' +')[0],
			tweetUrl: parseForUrl(tweet.text),
			icon: tweet.user.profile_image_url,
		};
	}).filter((tweet) => {
		return !tweet.isReply;
	});
}

function parseForUrl(tweetText) {
	const link = new RegExp('(http.*)');
	const m = tweetText.match(link);
	if (!m) {
		return null;
	}
	return m[0].split(' ')[0];
}

function addLinkToText(text, link) {
	const find = text.match(link);
	if (!find) {
		return text;
	}
	let textArr = text.split('');
	textArr.splice(find.index, 0, `<a href="${link}">`);
	textArr.splice(find.index + link.length + 1, 0, `</a>`);
	text = textArr.join('');
	return text;
}