const recentVideoURL = `http://138.68.243.184:8080/youtube/videoRecent?user=UC9Qo0Iz5OdE1KRIqJVkD93w`;
const tweetURL = `http://138.68.243.184:8080/twitter/recentTweets?username=endgameVR`;

const DEFAULT_VIDEO = "z3aHWMyo_10";
const DEFAULT_VIDEO_TEXT = "Could not load most recent video. Enjoy this episode from 2019:";

applyEndgameVideo();
applyTweets();

// get most recent video of Endgame and display it
function applyEndgameVideo() {
	makeHTTPRequest(recentVideoURL, 'GET', function () {
        const iframe = document.createElement('iframe');
        iframe.id = 'videoIFrame';
		if (this.status && this.status > 199 && this.status < 300) {
            const videoID = this.responseText;
            iframe.src = `https://www.youtube.com/embed/${videoID}`
		} else {
			const p = document.querySelector("#most_recent_episode");
			if (p) {
				p.innerHTML = DEFAULT_VIDEO_TEXT;
                iframe.src = `https://www.youtube.com/embed/${DEFAULT_VIDEO}`;
			}
		}
        document.getElementById("video").appendChild(iframe);
	});
}

function applyTweets() {
	makeHTTPRequest(tweetURL, 'GET', function () {
		const tweets = parseTweets(this.responseText);
		document.getElementById('twitterWidget').innerHTML = makeTwitterWidget(tweets);
	});
}

// GENERAL UTILITY FUNCTIONS
// -------------------------

function makeHTTPRequest(url, method, callback, errorCallback) {
	let request = new XMLHttpRequest();
	request.open(method, url);
	request.onload = callback;
	request.onerror = errorCallback || displayError;
	request.send();
}

function displayError() {
	console.log('An error occurred:', this);
}

// UI FUNCTIONS
// ------------
