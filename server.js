const http = require('http');
const PUBLIC_DIR = 'public';
const PORT = 80;
const server = http.createServer((request, response) => {
	response.setHeader('Access-Control-Allow-Origin', '*');

	const url = require('url');
	const requestURL = url.parse(request.url, true);
	const route = getDestination(requestURL.pathname);

	if (!route) {
		// send message saying request is denied (invalid request?)
		response.statusCode = 403;
		response.write('Request denied.');
		response.end();
		return;
	}

	response.setHeader('Content-Type', setContentType(route));
	servePage(route, response);
});
server.listen(PORT, () => {
	console.log(`Endgame server listening on port ${PORT}.`);
});
server.on('error', (err) => {
	console.log('Endgame server has encountered an error:', err);
});

function getDestination(pathname) {
	console.log(`${timestamp()} Received request for pathname ${pathname}`);
	if (pathname === '/') {
		pathname = '/index.html';
	}

	// check for directory traversal
	const path = require('path');
	const destination = path.join(__dirname, PUBLIC_DIR, pathname);
	if (!destination.includes(__dirname)) {
		console.log('someone tried to traverse above this directory.');
		return null;
	}

	return destination;
}

function setContentType(path) {
	let p = path.split('.');
	switch (p[p.length - 1]) {
		case 'svg':
			return 'image/svg+xml';
		case 'html':
			return 'text/html';
		case 'png':
			return 'image/png';
	}
	return 'text/xml';
}

function servePage(page, response) {
	try {
		var fs = require('fs');
		var rs = fs.createReadStream(page);
		rs.on('error', function (err) {
			console.log(`Sending a 404 Not Found in response to request at ${page}`);
			response.writeHead(404, `Not Found`);
			response.write(`The requested file was not found.`);
			response.end();
		});
		rs.pipe(response);
		rs.on('end', function () {
			response.end();
		});
	}
	catch (err) {
		console.log(`An unknown error was caught when trying to retrieve ${page}`);
		response.writeHead(404, `There was an error accessing the file at ${page}`);
		response.end();
	}
}

function timestamp() {
	return `${new Date().toUTCString()} >`;
}
